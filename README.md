# Quasar App

Esse projeto é o frontend de um aplicativo semelhante a um blog, implementado com vue.js e Quasar utilizando de um API criado usando Laravel. O código do API pode ser visto em <https://github.com/MarcoAntonioPaixao/blog> dentro do diretório routes no arquivo api.php.

O objetivo deste projeto é servir como exemplo de como se estruturar um projeto usando Quasar, dando ênfase especial em como realizar testes (por enquanto apenas unitários) nesta framework.
