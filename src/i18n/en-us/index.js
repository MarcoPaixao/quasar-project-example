export default {
  failed: "Action failed",
  success: "Action was successful",
  greeting: "Welcome {0}",
  selectLanguageMessage: "Language",
  aboutTitle: "About me",
  userSettingsTitle: "User Settings",
  settingsLabel: "Settings",
  readMoreLabel: "Read More",
  commentsLabel: "Comments"
};
