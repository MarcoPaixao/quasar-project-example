export default {
  failed: "Accion: Fallida",
  success: "La acción fue exitosa",
  greeting: "Bienvenido {0}",
  selectLanguageMessage: "Idioma",
  aboutTitle: "Sobre mi",
  userSettingsTitle: "Ajustes de usuario",
  settingsLabel: "Ajustes",
  readMoreLabel: "Lee mas",
  commentsLabel: "Comentarios"
};
