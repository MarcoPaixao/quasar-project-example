export default {
  failed: "A ação falhou",
  success: "Ação completada com sucesso",
  greeting: "Bem-vindo {0}",
  selectLanguageMessage: "Linguagem",
  aboutTitle: "Sobre quem eu sou",
  userSettingsTitle: "Configurações de Usuário",
  settingsLabel: "Configurações",
  readMoreLabel: "Leia Mais",
  commentsLabel: "Comentários"
};
