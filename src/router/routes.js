// const routes = [
//   {
//     path: "/",
//     component: () => import("layouts/MainLayout.vue"),
//     children: [
//       { path: "", component: () => import("pages/articles-preview.vue") }
//     ]
//   },
//   {
//     path: "/article/:articleID",
//     component: () => import("layouts/MainLayout.vue"),
//     children: [
//       {
//         path: "",
//         component: () => import("pages/article-details.vue"),
//         props: true
//       }
//     ]
//   },
//   {
//     path: "/login",
//     component: () => import("layouts/MainLayout.vue"),
//     children: [{ path: "", component: () => import("pages/user-login.vue") }]
//   },
//   {
//     path: "/register",
//     component: () => import("layouts/MainLayout.vue"),
//     children: [
//       { path: "", component: () => import("pages/user-registration.vue") }
//     ]
//   },
//   {
//     path: "/about",
//     component: () => import("layouts/MainLayout.vue"),
//     children: [{ path: "", component: () => import("pages/about-page.vue") }]
//   }
// ];
import MainLayout from "../layouts/main-layout.vue";
import ArticlesPreview from "../pages/articles-preview";
import ArticleDetails from "../pages/article-details";
import UserLogin from "../pages/user-login";
import UserRegistration from "../pages/user-registration";
import AboutPage from "../pages/about-page";

const routes = [
  {
    path: "/",
    component: MainLayout,
    children: [{ path: "", component: ArticlesPreview }]
  },
  {
    path: "/article/:articleID",
    component: MainLayout,
    children: [
      {
        path: "",
        component: ArticleDetails,
        props: true
      }
    ]
  },
  {
    path: "/login",
    component: MainLayout,
    children: [{ path: "", component: UserLogin }]
  },
  {
    path: "/register",
    component: MainLayout,
    children: [{ path: "", component: UserRegistration }]
  },
  {
    path: "/about",
    component: MainLayout,
    children: [{ path: "", component: AboutPage }]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
