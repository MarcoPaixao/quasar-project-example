export function fetchArticlesSummaries({ commit }) {
  return new Promise((resolve, reject) => {
    this.$axios
      .get("http://articles.com/api/article")
      .then(res => {
        const articlesData = {
          articlesPreview: res.data.data,
          pagination: res.data.links,
          metaData: res.data.meta
        };
        resolve(articlesData);
      })
      .catch(err => {
        reject(err);
      });
  });
}

export function fetchArticleCategories({ commit }, parameters) {
  return new Promise((resolve, reject) => {
    this.$axios
      .get(`http://articles.com/api/article/tag/${parameters.id}`)
      .then(res => {
        resolve(res.data.data);
      })
      .catch(err => {
        reject(err);
      });
  });
  // this.$axios
  //   .get(`/api/article/tag/${id}`)
  //   .then(res => {
  //     console.log({ res });
  //   })
  //   .catch(err => console.log("Error on loading article categories: " + err));
}

export function fetchArticleDetails({ commit }, parameters) {
  return new Promise((resolve, reject) => {
    this.$axios
      .get(`http://articles.com/api/article/${parameters.id}`)
      .then(res => {
        resolve(res.data.data);
      })
      .catch(err => reject(err));
  });
}

export function fetchArticleComments({ commit }, parameters) {
  return new Promise((resolve, reject) => {
    this.$axios
      .get(`http://articles.com/api/comment/${parameters.id}`)
      .then(res => {
        resolve(res.data.data);
      })
      .catch(err => reject(err));
  });
}
