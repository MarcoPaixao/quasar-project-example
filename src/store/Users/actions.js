export function fetchUserName({ commit }, parameters) {
  return new Promise((resolve, reject) => {
    this.$axios
      .get(`http://articles.com/api/user/name/${parameters.id}`)
      .then(res => {
        resolve(res.data.userName);
      })
      .catch(err => reject(err));
  });
}

export function fetchToken({ commit }, parameters) {
  return new Promise((resolve, reject) => {
    this.$axios
      .get("http://articles.com/api/oauth/token", {
        params: {
          login: parameters.login,
          password: parameters.password
        }
      })
      .then(res => {
        resolve(res.data);
      })
      .catch(err => reject(err));
  });
}

export function fetchUserData({ commit }, parameters) {
  return new Promise((resolve, reject) => {
    this.$axios
      .get("http://articles.com/api/user/data", {
        params: {
          login: parameters.login
        }
      })
      .then(res => {
        resolve(res.data.userData);
      })
      .catch(err => reject(err));
  });
}

export function registerUser({ commit }, parameters) {
  if (parameters.roles === undefined) {
    return new Promise((resolve, reject) => {
      this.$axios
        .post("http://articles.com/api/register", {
          userName: parameters.userName,
          userEmail: parameters.userEmail,
          userPassword: parameters.userPassword
        })
        .then(() => {
          resolve();
        })
        .catch(err => reject(err));
    });
  } else {
    return new Promise((resolve, reject) => {
      this.$axios
        .post("http://articles.com/api/register/role", {
          userName: parameters.userName,
          userEmail: parameters.userEmail,
          userPassword: parameters.userPassword,
          roles: parameters.roles
        })
        .then(() => {
          resolve();
        })
        .catch(err => reject(err));
    });
  }
}
