export function getUserType(state) {
  return state.userType;
}

export function getUserName(state) {
  return state.userName;
}

export function getUserID(state) {
  return state.userID;
}
