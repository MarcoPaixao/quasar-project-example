export function setUserType(state, payload) {
  state.userType = payload;
}

export function setUserName(state, payload) {
  state.userName = payload;
}

export function setUserID(state, payload) {
  state.userID = payload;
}
