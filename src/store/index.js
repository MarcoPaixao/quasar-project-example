import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import articles from "./Articles/index";
import users from "./Users/index";

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      articles,
      users
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  const $axios = axios.create();
  Store.$axios = $axios;

  if (window.Cypress) {
    window.__store__ = Store;
  }

  return Store;
}
