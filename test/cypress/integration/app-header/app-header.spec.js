import * as ctx from "../../../../quasar.conf.js";

describe("Landing", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it(".should() - assert that the home title has rendered Home", () => {
    cy.get("[data-test='homeTitle']").should("contain", "Home");
  });
});

describe("app-header tests", () => {
  beforeEach(() => {
    // reseta o estado entre testes, é como abri uma nova aba no browser
    cy.visit("/");
  });

  it("goes to the about me page when the about me link is clicked", () => {
    cy.get("[data-test='aboutMeTitle']").click();
    cy.url().should("include", "/about");
  });

  it("goes to the log in page when the login button is clicked", () => {
    cy.get("[data-test='loginButton']").click();
    cy.url().should("include", "/login");
  });

  it("goes to the register page when the register button is clicked", () => {
    cy.get("[data-test='registerButton']").click();
    cy.url().should("include", "/register");
  });

  it("allows the user to logout after having logged in", () => {
    cy.loginAsAdmin();
    cy.get("[data-test='settingsButton']").click();
    cy.get("[data-test='logoutButton']").click();
    cy.get("[data-test='loginButton']").should("exist");
  });
});
