import * as ctx from "../../../../quasar.conf.js";

describe("Home page tests", () => {
  beforeEach(() => {
    // reseta o estado entre testes, é como abri uma nova aba no browser
    cy.visit("/");
  });

  it("should render 5 article-summary components", () => {
    cy.get("[data-test='articleSummary']").should("have.length", 5);
  });

  it("should go to the article page when the read more button in a article summary is clicked ", () => {
    cy.get("[data-test='readMoreButton']")
      .first()
      .click();
    cy.url().should("contain", "/article/");
  });
});
