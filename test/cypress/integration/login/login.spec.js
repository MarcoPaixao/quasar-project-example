import * as ctx from "../../../../quasar.conf.js";

describe("Login page tests", () => {
  beforeEach(() => {
    // reseta o estado entre testes, é como abrir uma nova aba no browser
    cy.visit("/");
  });

  it("user can login as reader", () => {
    cy.get("[data-test='loginButton']").click();

    cy.get("[data-test='userNameField']").type("reader");
    cy.get("[data-test='passwordField']").type("123");

    cy.get("[data-test='submitButton']").click();

    cy.url().should("include", "/");
  });

  it("user can login as admin", () => {
    cy.get("[data-test='loginButton']").click();

    cy.get("[data-test='userNameField']").type("admin");
    cy.get("[data-test='passwordField']").type("123");
    cy.get("[data-test='submitButton']").click();
    cy.get("[data-test='settingsButton']").click();

    cy.get("[data-test='registerNewUserButton']").should("exist");
  });
});
