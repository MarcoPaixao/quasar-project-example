/* eslint-disable */
/**
 * @jest-environment jsdom
 */
import { mount, createLocalVue, shallowMount } from "@vue/test-utils";
import * as All from "quasar";
import Vuex from "vuex";
import messages from "../../../src/i18n";
import ArticleCommment from "../../../src/components/article-comment";
import VueI18n from "vue-i18n";
import axios from "axios";
import flushPromises from "flush-promises";
const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});

const localVue = createLocalVue();
localVue.use(Quasar, { components });
localVue.use(VueI18n);
localVue.use(Vuex);

describe("article-comment", () => {
  let i18n;
  let store;
  let actions;
  beforeEach(() => {
    actions = {
      fetchUserName: jest.fn(({ commit }, parameters) => {
        return new Promise((resolve, reject) => {
          if (parameters.id === 1) resolve("alexey");
          else if (parameters.id === 2) resolve("boris");
          else reject("error");
        });
      })
    };

    store = new Vuex.Store({
      modules: {
        users: {
          state: {},
          actions: actions,
          namespaced: true
        }
      }
    });

    const $axios = axios.create();
    store.$axios = $axios;

    i18n = new VueI18n({
      locale: "pt-br",
      fallbackLocale: "pt-br",
      messages
    });
  });

  it("passes the sanity test", () => {
    expect(true).toBe(true);
  });

  it("calls the fetchUserName method", async () => {
    const firstComment = await mount(ArticleCommment, {
      propsData: {
        authorID: 1,
        date: "27/05/1992",
        content: "lorem ipsum dolor"
      },
      localVue,
      i18n,
      store
    });

    await flushPromises();
    expect(actions.fetchUserName).toHaveBeenCalled();
  });

  it("renders correctly the given data", async () => {
    const firstComment = await mount(ArticleCommment, {
      propsData: {
        authorID: 1,
        date: "1992-05-27 14:33:45",
        content: "lorem ipsum dolor"
      },
      localVue,
      i18n,
      store
    });

    await flushPromises();
    expect(firstComment.find("[data-test='authorName']").text()).toBe(
      "alexey at 1992-05-27 14:33:45"
    );
    expect(firstComment.find("[data-test='commentContent']").text()).toBe(
      "lorem ipsum dolor"
    );
  });
});
