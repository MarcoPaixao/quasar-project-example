/* eslint-disable */
/**
 * @jest-environment jsdom
 */
import { mount, createLocalVue, shallowMount } from "@vue/test-utils";
import * as All from "quasar";
import Vuex from "vuex";
import messages from "../../../src/i18n";
import ArticleSummary from "../../../src/components/article-summary";
import VueI18n from "vue-i18n";
import axios from "axios";
import flushPromises from "flush-promises";
const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});

const localVue = createLocalVue();
localVue.use(Quasar, { components });
localVue.use(VueI18n);
localVue.use(Vuex);

describe("article-summary", () => {
  let i18n;
  let store;
  let actions;
  beforeEach(() => {
    actions = {
      fetchArticleCategories: jest.fn(({ commit }, parameters) => {
        return new Promise((resolve, reject) => {
          if (parameters.id === 1) {
            resolve([{ name: "laravel" }, { name: "vue" }]);
          } else if (parameters.id === 2) {
            resolve([{ name: "jest" }, { name: "vue" }, { name: "vuex" }]);
          } else {
            reject(err);
          }
        });
      })
    };

    store = new Vuex.Store({
      modules: {
        articles: {
          state: {},
          actions: actions,
          namespaced: true
        }
      }
    });

    const $axios = axios.create();
    store.$axios = $axios;

    i18n = new VueI18n({
      locale: "pt-br",
      fallbackLocale: "pt-br",
      messages
    });
  });

  it("passes the sanity test", () => {
    expect(true).toBe(true);
  });

  it("calls the fetchArticleCategories method", async () => {
    const firstSummary = await mount(ArticleSummary, {
      propsData: {
        title: "how to test vue",
        articleID: 1
      },
      localVue,
      i18n,
      store
    });

    await flushPromises();
    expect(actions.fetchArticleCategories).toHaveBeenCalled();
  });

  it("renders correctly the given data", async () => {
    const firstSummary = await mount(ArticleSummary, {
      propsData: {
        title: "how to test vue",
        articleID: 1
      },
      localVue,
      i18n,
      store
    });

    await flushPromises();
    expect(firstSummary.find("[data-test='summaryTitle']").text()).toBe(
      "how to test vue"
    );
    const firstSummaryCategories = firstSummary.findAll("[data-test='badge']");
    expect(firstSummaryCategories.at(0).text()).toBe("laravel");
    expect(firstSummaryCategories.at(1).text()).toBe("vue");
  });
});
