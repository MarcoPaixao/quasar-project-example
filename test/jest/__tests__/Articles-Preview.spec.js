/* eslint-disable */
/**
 * @jest-environment jsdom
 */

import { mount, createLocalVue, shallowMount } from "@vue/test-utils";
import * as All from "quasar";
import VueRouter from "vue-router";
import routes from "../../../src/router/routes";
import Vuex from "vuex";
import articles from "../../../src/store/Articles/index";
import users from "../../../src/store/Users/index";
import messages from "../../../src/i18n";
import MainLayout from "../../../src/layouts/main-layout.vue";
import ArticlesPreview from "../../../src/pages/articles-preview";
import ArticleSummary from "../../../src/components/article-summary";
import VueI18n from "vue-i18n";
import axios from "axios";
const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});

const localVue = createLocalVue();
localVue.use(Quasar, { components });
localVue.use(VueI18n);
localVue.use(Vuex);
localVue.use(VueRouter);

describe("Articles Preview", () => {
  let store;
  let router;
  let i18n;
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        articles,
        users
      }
    });

    const $axios = axios.create();
    store.$axios = $axios;

    router = new VueRouter({
      routes
    });

    i18n = new VueI18n({
      locale: "pt-br",
      fallbackLocale: "pt-br",
      messages
    });
  });

  it("passes the sanity test", () => {
    expect(true).toBe(true);
  });

  it("renders under main-layout", () => {
    // all pages must be under a layout!!!
    const mainWrapper = mount(MainLayout, {
      localVue,
      i18n,
      store,
      router
    });

    const previewWrapper = mainWrapper.find(ArticlesPreview);
    expect(previewWrapper.is(ArticlesPreview)).toBe(true);
  });
});
