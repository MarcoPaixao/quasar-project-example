/* eslint-disable */
/**
 * @jest-environment jsdom
 */

import { mount, createLocalVue, shallowMount } from "@vue/test-utils";
import * as All from "quasar";
import VueRouter from "vue-router";
import routes from "../../../src/router/routes";
import Vuex from "vuex";
import articles from "../../../src/store/Articles/index";
import users from "../../../src/store/Users/index";
import messages from "../../../src/i18n";
import LanguageSelector from "../../../src/components/language-selector";
import VueI18n from "vue-i18n";
import axios from "axios";
const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});

const localVue = createLocalVue();
localVue.use(Quasar, { components });
localVue.use(VueI18n);
localVue.use(Vuex);
localVue.use(VueRouter);

describe("language-selector", () => {
  let store;
  let router;
  let i18n;
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        articles,
        users
      }
    });

    const $axios = axios.create();
    store.$axios = $axios;

    router = new VueRouter({
      routes
    });

    i18n = new VueI18n({
      locale: "pt-br",
      fallbackLocale: "pt-br",
      messages
    });
  });

  it("passes the sanity test", () => {
    expect(true).toBe(true);
  });

  // test that the language is changed when the selector button is manipulated
  it("changes the message when the language is changed from portuguese to spanish", () => {
    const mainWrapper = shallowMount(LanguageSelector, {
      localVue,
      i18n,
      store,
      router
    });

    // const selector = mainWrapper.find("#selector");
    // // because I'm using quasar I cannot use this method on the element
    // selector.setValue("pt-br");

    const languageDiv = mainWrapper.find("[data-test='selectLanguageMessage']");
    expect(languageDiv.text()).toBe("Linguagem");

    // mainWrapper.setData({ currentLang: "es-es" });

    // // selector.setValue("es-es");
    mainWrapper.vm.$i18n.locale = "es-es";
    expect(languageDiv.text()).toBe("Idioma");
  });
});
