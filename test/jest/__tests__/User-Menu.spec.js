/* eslint-disable */
/**
 * @jest-environment jsdom
 */

import { mount, createLocalVue, shallowMount } from "@vue/test-utils";
import * as All from "quasar";
import messages from "../../../src/i18n";
import UserMenu from "../../../src/components/user-menu";
import VueI18n from "vue-i18n";
import flushPromises from "flush-promises";
const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});

const localVue = createLocalVue();
localVue.use(Quasar, { components });
localVue.use(VueI18n);

describe("user-menu", () => {
  let i18n;
  beforeEach(() => {
    i18n = new VueI18n({
      locale: "pt-br",
      fallbackLocale: "pt-br",
      messages
    });
  });

  it("passes the sanity test", () => {
    expect(true).toBe(true);
  });

  it("only shows the login and register button when user is of type guest", () => {
    const firstMenu = shallowMount(UserMenu, {
      propsData: {
        userRoles: ["guest", "admin"]
      },
      localVue,
      i18n
    });

    expect(firstMenu.find("[data-test='loginButton']").exists()).toBe(true);
    expect(firstMenu.find("[data-test='registerButton']").exists()).toBe(true);

    const secondMenu = shallowMount(UserMenu, {
      propsData: {
        userRoles: ["reader", "writer", "admin"]
      },
      localVue,
      i18n
    });

    expect(secondMenu.find("[data-test='loginButton']").exists()).toBe(false);
    expect(secondMenu.find("[data-test='registerButton']").exists()).toBe(
      false
    );

    const thirdMenu = shallowMount(UserMenu, {
      propsData: {
        userRoles: ["reader", "guest", "admin"]
      },
      localVue,
      i18n
    });

    expect(thirdMenu.find("[data-test='loginButton']").exists()).toBe(true);
    expect(thirdMenu.find("[data-test='registerButton']").exists()).toBe(true);
  });

  it("emits the requestPageChange event when the login button is clicked", () => {
    const firstMenu = shallowMount(UserMenu, {
      propsData: {
        userRoles: ["guest"]
      },
      localVue,
      i18n
    });

    // const spy = jest.spyOn(firstMenu.vm, "requestPageChangeToLogin");
    // expect(spy).toHaveBeenCalled();

    // necessary because it's a q-btn and not a normal btn!!!
    const loginButtonWrapper = firstMenu.find("[data-test='loginButton']");
    loginButtonWrapper.vm.$options._parentListeners.click();
    // loginButtonWrapper.vm._events.click(); --this is not a function!!!
    expect(firstMenu.emitted().requestPageChange).toBeTruthy();
  });

  it("emits the requestPageChange event when the register button is clicked", () => {
    const firstMenu = shallowMount(UserMenu, {
      propsData: {
        userRoles: ["guest"]
      },
      localVue,
      i18n
    });

    const registerButtonWrapper = firstMenu.find(
      "[data-test='registerButton']"
    );
    registerButtonWrapper.vm.$options._parentListeners.click();
    // loginButtonWrapper.vm._events.click(); --this is not a function!!!
    expect(firstMenu.emitted().requestPageChange).toBeTruthy();
  });
});
