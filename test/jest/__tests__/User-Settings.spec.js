/* eslint-disable */
/**
 * @jest-environment jsdom
 */

import { mount, createLocalVue, shallowMount } from "@vue/test-utils";
import * as All from "quasar";
import VueRouter from "vue-router";
import routes from "../../../src/router/routes";
import Vuex from "vuex";
import articles from "../../../src/store/Articles/index";
import users from "../../../src/store/Users/index";
import messages from "../../../src/i18n";
import UserSettings from "../../../src/components/user-settings";
import VueI18n from "vue-i18n";
import axios from "axios";
const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];
  if (val && val.component && val.component.name != null) {
    object[key] = val;
  }
  return object;
}, {});

const localVue = createLocalVue();
localVue.use(Quasar, { components });
localVue.use(VueI18n);
localVue.use(Vuex);
localVue.use(VueRouter);

describe("user-settings", () => {
  let store;
  let router;
  let i18n;
  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        articles,
        users
      }
    });

    const $axios = axios.create();
    store.$axios = $axios;

    router = new VueRouter({
      routes
    });

    i18n = new VueI18n({
      locale: "pt-br",
      fallbackLocale: "pt-br",
      messages
    });
  });

  it("passes the sanity test", () => {
    expect(true).toBe(true);
  });

  it("does not render the register new user button when user does not have the admin role", () => {
    const mainWrapper = shallowMount(UserSettings, {
      propsData: {
        userRoles: ["guest", "reader", "writer"]
      },
      localVue,
      i18n,
      store,
      router
    });

    expect(
      mainWrapper.find("[data-test='registerNewUserButton']").exists()
    ).toBe(false);
  });

  it("renders the register new user button when user has the admin role", () => {
    const mainWrapper = shallowMount(UserSettings, {
      propsData: {
        userRoles: ["reader", "admin", "writer"]
      },
      localVue,
      i18n,
      store,
      router
    });

    expect(
      mainWrapper.find("[data-test='registerNewUserButton']").exists()
    ).toBe(true);
  });
});
